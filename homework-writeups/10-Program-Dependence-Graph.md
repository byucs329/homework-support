# Objectives

Create program dependence graphs (PDG) for methods.

# Reading

See [program dependence graph](https://bitbucket.org/byucs329/byu-cs-329-lecture-notes/src/master/program-dependence-graph/).

# Problems

Compute the PDG for the two following pieces of code. Show each step of the computation:

  1. Reaching definitions
  2. Augmented CFG
  4. Post-dominator tree
  5. Control dependency graph
  6. Data dependency edges in the form of a list for each variable: **do not add the edges to the control dependance graph**---make a list

Be sure to add an `Entry` and `Exit` node to the control flow graphs. Use the numbers in the comments for the nodes in the graphs. **Show all the work.** There should be a final table for every graph: reaching definitions, post-dominator tree, and control dependance.

Here is the code.

1. (**50 points**) Please use the numbers in the comments for the block numbers in the graphs for ease in grading. Data-dependance edges should be in a list for each variable.

```java
read (n) // 1
if (n < 0) {
    n := 0 // 9
} // 2
i := 1 // 3
sum := 0 // 4
product := 1  // 5
while i <= n {
    sum := sum + i // 10
    product := product * i // 11
    i := i + 1 // 12
} // 6
write (sum) // 7
write (product) // 8
```

2. (**50 points**) Please use the numbers in the comments for the block numbers in the graphs for ease in grading. Data-dependance edges should be in a list for each variable.

```java
read a  // 1
if (a <= 0) {
  print a // 6
  return // 7
} // 2
i = a // 3
while (i > 0) {
  a := a + i // 8
  i := i - 1 // 9
} // 4
print a // 5
```

# Submission

Upload a PDF of your work to [canvas.byu.edu](http://canvas.byu.edu).